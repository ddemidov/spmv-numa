#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

//---------------------------------------------------------------------------
inline int read_problem(const std::string &fname,
        std::vector<int>    &row,
        std::vector<int>    &col,
        std::vector<double> &val
        )
{
    std::cout << "Reading \"" << fname << "\"..." << std::endl;
    std::ifstream f(fname.c_str(), std::ios::binary);
    if (!f) throw std::runtime_error("Failed to open problem file");

    int n;

    f.read((char*)&n, sizeof(int));

    row.resize(n + 1);
    f.read((char*)row.data(), row.size() * sizeof(int));

    col.resize(row.back());
    val.resize(row.back());

    f.read((char*)&col[0], col.size() * sizeof(int));
    f.read((char*)&val[0], val.size() * sizeof(double));

    std::cout << "Done\n" << std::endl;

    return n;
}

typedef std::chrono::high_resolution_clock Clock;

double seconds(typename Clock::time_point tic, typename Clock::time_point toc) {
    return static_cast<double>(Clock::duration::period::num)
        * typename Clock::duration(toc - tic).count()
        / Clock::duration::period::den;

}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    // Read matrix from a binary file.
    std::vector<int>    row;
    std::vector<int>    col;
    std::vector<double> val;

    int n = read_problem("problem.dat", row, col, val);

    std::vector<double> x(n), y(n, 0);

    std::default_random_engine rng(0);
    std::uniform_real_distribution<double> rnd(0, 1);

    std::generate(x.begin(), x.end(), [&]() { return rnd(rng); });

    const int nexp = 16;

    // Do a barrel roll
    auto tic = Clock::now();
    for(int exp = 0; exp < nexp; ++exp) {
#pragma omp parallel for
        for(int i = 0; i < n; ++i) {
            for(int j = row[i], e = row[i+1]; j < e; ++j)
                y[i] += val[j] * x[ col[j] ];
        }
    }
    auto toc = Clock::now();

    std::cout << seconds(tic, toc) << " sec" << std::endl;
}
